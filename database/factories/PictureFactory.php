<?php

namespace Database\Factories;

use App\Models\Country;
use App\Models\Writer;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Picture>
 */
class PictureFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => fake()->unique()->name(),
            'description' => fake()->text(),
            'creation_date' => fake()->date(),
            'country_id' => Country::inRandomOrder()->first(),
            'writer_id' => Writer::inRandomOrder()->first()
        ];
    }
}
