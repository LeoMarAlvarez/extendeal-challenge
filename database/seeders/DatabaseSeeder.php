<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Picture;
use App\Models\Writer;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        Country::factory()->count(15)->create();
        Writer::factory()->count(10)->create();
        Picture::factory()->count(30)->create();
    }
}
