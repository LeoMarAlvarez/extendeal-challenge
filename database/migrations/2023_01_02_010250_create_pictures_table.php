<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pictures', function (Blueprint $table) {
            $table->id();
            $table->string('name', 150)->unique();
            $table->longText('description');
            $table->date('creation_date');
            $table->unsignedBigInteger('country_id');
            $table->unsignedBigInteger('writer_id');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('writer_id')->references('id')->on('writers');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pictures');
    }
};
