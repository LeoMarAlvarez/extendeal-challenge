<?php

namespace App\Rules;

use App\Models\Country;
use Illuminate\Contracts\Validation\InvokableRule;
use Illuminate\Support\Arr;

class FilterPictureRule implements InvokableRule
{
    /**
     * Run the validation rule.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     * @return void
     */
    public function __invoke($attribute, $value, $fail)
    {
        if (!isset($value['country'])) {
            $fail('Filters available are country');
            return false;
        }
        if (Country::where('code_iso', $value['country'])->whereNull('deleted_at')->exists()) {
            return true;
        } else {
            $fail('Not exist Country');
        }
    }
}
