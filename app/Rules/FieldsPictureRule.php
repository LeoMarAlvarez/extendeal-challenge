<?php

namespace App\Rules;

use App\Models\Picture;
use Illuminate\Contracts\Validation\Rule;

class FieldsPictureRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $fields = explode(',', $value);
        foreach ($fields as $field) {
            if (!in_array($field, Picture::fieldsAvailable())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The fields availables are: ' . implode(',', Picture::fieldsAvailable());
    }
}
