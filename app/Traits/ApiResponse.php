<?php

namespace App\Traits;

Trait ApiResponse {

    protected static function success(
        $data,
        $status = 200,
        $title = 'Success'
    ) {
        return response()->json([
            'title' => $title,
            'data' => $data
        ], $status);
    }

    protected static function error(
        $msg,
        $status = 422,
        $title = 'Fail'
    ) {
        return response()->json(
            [
                'error' => $msg,
                'title' => $title
            ],
            $status
        );
    }
}