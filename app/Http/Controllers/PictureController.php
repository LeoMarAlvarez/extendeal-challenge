<?php

namespace App\Http\Controllers;

use App\Http\Requests\FilterPictureRequest;
use App\Http\Requests\PictureRequest;
use App\Http\Resources\PictureResoucerCollection;
use App\Http\Resources\PictureResource;
use App\Models\Picture;
use Illuminate\Http\Request;
use App\Traits\ApiResponse;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;

class PictureController extends Controller
{
    use ApiResponse;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FilterPictureRequest $request): JsonResponse
    {
        return $this::success(new PictureResoucerCollection(
            Picture::when(isset($request->filters['country']), function (Builder $qy) use ($request) {
                $qy->whereHas('country', fn ($q) => $q->where('code_iso', $request->filters['country']));
            })->paginate(request('per_page') ?? 15)
        ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PictureRequest $request): JsonResponse
    {
        $picture = Picture::create($request->all());
        return $this::success(new PictureResource($picture),201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function show(Picture $picture)
    {
        return $this::success(new PictureResource($picture));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function update(PictureRequest $request, Picture $picture)
    {
        $picture->update($request->all());
        $picture->save();
        return $this::success(new PictureResource($picture));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Picture  $picture
     * @return \Illuminate\Http\Response
     */
    public function destroy(Picture $picture)
    {
        return $this::success([
            'deleted' => (bool) $picture->delete()
        ], 200);
    }
}
