<?php

namespace App\Http\Resources;

use App\Models\Picture;
use Illuminate\Http\Resources\Json\JsonResource;

class PictureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $fields = request('fields') ?
            explode(',', request('fields')) :
            Picture::fieldsAvailable();

        $arr = array();
        foreach ($fields as $field) {
            $arr[$field] = $this->$field;
        }

        return $arr;
    }
}
