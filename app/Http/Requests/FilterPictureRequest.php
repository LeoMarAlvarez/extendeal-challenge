<?php

namespace App\Http\Requests;

use App\Rules\FieldsPictureRule;
use App\Rules\FilterPictureRule;
use Illuminate\Foundation\Http\FormRequest;

class FilterPictureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return request()->isJson();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'filters' => ['array', new FilterPictureRule],
            'fields' => ['required', new FieldsPictureRule]
        ];
    }
}
