<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PictureRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return request()->isJson();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', Rule::unique('pictures')->ignore($this->route('picture'))],
            'creation_date' => 'required|date|date_format:Y-m-d',
            'description' => 'required',
            'country_id' => ['required', Rule::exists('countries', 'id')->whereNull('deleted_at')],
            'writer_id' => ['required', Rule::exists('writers', 'id')->whereNull('deleted_at')]
        ];
    }
}
