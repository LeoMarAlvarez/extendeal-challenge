<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Picture extends Model
{
    use HasFactory, SoftDeletes;

    protected $conection = 'mysql';
    protected $table = 'pictures';

    protected $fillable = [
        'name',
        'description',
        'creation_date',
        'country_id',
        'writer_id'
    ];

    public $dates = [
        'creation_date'
    ];

    public static function fieldsAvailable()
    {
        return [
            'id',
            'name',
            'date',
            'description',
            'country_name',
            'writer_name'
        ];
    }

    public function country() {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function writer() {
        return $this->belongsTo(Writer::class, 'writer_id');
    }

    protected function CountryName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->country->name . ' (' . $this->country->code_iso . ')'
        );
    }

    protected function WriterName(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->writer->last_name . ' ' . $this->writer->last_name
        );
    }

    protected function date(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->creation_date->format('d-m-Y')
        );
    }
}
