# Extendeal Challenge

### Pasos para levantar Proyecto

1.  clonar repo
    * git clone https://gitlab.com/LeoMarAlvarez/extendeal-challenge.git
2.  correr composer install
    * cd extendeal-challenge/
    * composer install
3.  Configurar Environment
    * cp .env.example .env
    * establecer datos de conexión a la base de datos
        * DB_CONNECTION=mysql
        * DB_HOST=mysql
        * DB_PORT=3306
        * DB_DATABASE=extendeal_challenge
        * DB_USERNAME=root
        * DB_PASSWORD=root
4.  Correr migraciones
    * php artisan migrate --seed
5.  importar Collection de Postman
    * ubicada en tests/Postman